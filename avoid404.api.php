<?php 

/**
 * @file
 * Hooks provided by the avoid404 module.
 */

/**
 * This hook is invoked during search an similar alias.
 *
 * This hook can be called to add aliases, which shall be avoided from 404.
 * @return array
 *   Array with aliases.
 */
function hook_avoid404_add_aliases() {
  return array();
}
