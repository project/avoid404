<?php 

/**
 * @file
 * Miscellaneous functions for avoid404.
 */

/**
 * Get menus selected in admin configuration.
 */
function avoid404_get_selected_menu() {
  $ret_menu = array();
  $menu = variable_get('avoid404_menus', array('main-menu'));
  sort($menu);
  foreach ($menu as $m) {
    if ($m == '0') {
      continue;
    }
    $ret_menu[] = $m;
  }
  return $ret_menu;
}

/**
 * Get all aliases that have to be considered in similarity check.
 *
 * @param array $menus
 *   All selected menus.
 */
function avoid404_get_all_aliases($menus) {
  $res = array();
  $settings_aliases = variable_get('avoid404_additional_aliases');
  foreach ($menus as $menu) {
    $menu = menu_tree_all_data($menu);
    $res = array_merge($res, _avoid404_get_all_aliases($menu));
  }
  $all_aliases = module_invoke_all('avoid404_add_aliases');
  $res = array_merge($res, $all_aliases);
  if (!empty($settings_aliases)) {
    $settings_aliases = explode("\n", $settings_aliases);
    foreach ($settings_aliases as $k) {
      $k = trim($k);
      if (!empty($k)) {
        $res[] = $k;
      }
    }
  }
  return $res;
}

/**
 * Helper function.
 *
 * @param array $menu
 *   Current Menu.
 */
function _avoid404_get_all_aliases($menu) {
  static $link_arr = array();
  foreach ($menu as $mn) {
    if ($mn['link']['hidden']) {
      continue;
    }
    $alias = drupal_get_path_alias($mn['link']['link_path']);
    if ($alias) {
      $link_arr[] = $alias;
    }
    if (isset($mn['below'])) {
      _avoid404_get_all_aliases($mn['below']);
    }
  }
  return $link_arr;
}

/**
 * Get the alias, to be redirected to.
 */
function avoid404_get_redirect_alias() {
  $alias_w = NULL;
  $aliases = avoid404_get_all_aliases(avoid404_get_selected_menu());
  $highest_p = 0;
  $settings_w = variable_get('avoid404_similarity', 50);
  foreach ($aliases as $alias) {
    similar_text($alias, request_path(), $probability);
    if ($probability > $highest_p) {
      $highest_p = $probability;
      $alias_w = $alias;
    }
  }
  if ($highest_p >= $settings_w) {
    return $alias_w;
  }
  else {
    return NULL;
  }
}

/**
 * Redirect to the alias path.
 */
function avoid404_goto() {
  $path = avoid404_get_redirect_alias();
  if ($path) {
    drupal_goto($path);
  }
}
