**Description:
This module is built to avoid 404 (page not found) error. 
This is done by automatically redirecting to the page with the 
most similar alias, compared to the one entered via URL.
To administer avoid404 go admin/config/search/avoid404.

**Notices:
Avoid404 has no dependencies to other modules.

**User permissions:
You have to give access rights to user roles for administer 
this module.

**API:
For more details see avoid404.api.php.
